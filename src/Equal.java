public class Equal extends Oper {

    public Equal(Exp l, Exp r){
        super(l ,r);
    }

    @Override
    protected String getOp() {
        return "==";
    }

    @Override
    public Type getType() {
        return null;
    }

    @Override
    public Value eval() {
        return Bool.valueOf(left.eval().getInt()==right.eval().getInt());
    }
}
