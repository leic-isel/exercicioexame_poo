public interface Value {
    public Type getType();
    public int getInt();
    public boolean equals(Value v);
}
