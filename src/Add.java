public class Add extends Oper {

    public Add(Exp l, Exp r){
        super(l ,r);
    }

    @Override
    protected String getOp() {
        return "+";
    }
    @Override
    public Type getType() {
        return Type.INT;
    }
    @Override
    public Value eval() {
        return new Int(left.eval().getInt() + right.eval().getInt());
    }

}
