public class Bool implements Value{
    public static Value TRUE = new Bool(1);
    public static Value FALSE = new Bool(0);

    private final int value;

    private Bool(int value){
        this.value = value;
    }

    public static Value valueOf(boolean b){
        return b ? TRUE : FALSE;
    }

    @Override
    public Type getType() {
        return Type.BOOL;
    }

    @Override
    public int getInt() {
        return value;
    }

    @Override
    public boolean equals(Value v) {
        return false;
    }

    public String toString() {
        return value == 1 ? "TRUE" : "FALSE";
    }
}