public class Int implements Value {
    private int i;
    public Int(int i) {
        this.i = i;
    }

    @Override
    public Type getType() {
        return Type.INT;
    }

    @Override
    public int getInt() {
        return i;
    }

    @Override
    public boolean equals(Value v) {
        return i == v.getInt();
    }

    @Override
    public String toString() {
        return ""+i;
    }
}
