public interface Exp {
    public Type getType();
    public String toString();
    public Value eval();
}
