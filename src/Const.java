public class Const implements Exp{

    protected Value value;

    public Const (Value value){
        this.value = value;
    }

    public String toString(){
        return value+"";
    }

    public Type getType(){
        return value.getType();
    }

    public Value eval(){
        return this.value;
    }

}
