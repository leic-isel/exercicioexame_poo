public class And extends Oper {

    public And(Exp l, Exp r){
        super(l ,r);
    }

    @Override
    protected String getOp() {
        return "&&";
    }

    @Override
    public Type getType() {
        return Type.BOOL;
    }

    @Override
    public Value eval() {
        return Bool.valueOf(left.eval().getInt()==right.eval().getInt());
    }
}
