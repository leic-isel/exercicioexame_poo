public class Program {
    public static void main(String[] args) {
        Var a = new Var ("A", Type.INT ),
            x = new Var("X", new Int(2));
        Exp b = new Var ("B", Bool.TRUE),
           c4 = new Const(new Int(4));
        Exp add = new Add(a, x); // a + x
        Exp e = new And(new Equal(add, c4), b);
        Int s = (Int) Type.INT.getValue();

        //System.out.println(s.getInt());
        //System.out.println(Type.BOOL.value());

        System.out.println(add+"="+add.eval());
        System.out.println(e+":"+e.getType()+"="+e.eval());
    }
}
