public enum Type {
    INT(new Int(0)), BOOL(Bool.FALSE);

    private final Value t;

    Type(Value def) {
        this.t = def;
    }

    public Value getValue(){
        return t;
    }

}
