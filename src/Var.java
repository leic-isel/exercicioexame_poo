public class Var extends Const {

    private String name;

    public Var(String name, Value value) {
        super(value);
        this.name=name;
    }

    public Var(String name, Type type){
        this(name, type.getValue());
        setValue(type.getValue());
    }

    public String toString(){
        return name;
    }

    public void setValue(Value value){
       super.value = value;
    }


}
