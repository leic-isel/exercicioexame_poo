public abstract class Oper implements Exp{

    protected Exp left, right;

    protected Oper(Exp left, Exp right){
        this.left = left;
        this.right = right;
    }

    public String toString(){
        return "("+left.toString()+getOp()+right.toString()+")";
    }

    protected abstract String getOp();

}
